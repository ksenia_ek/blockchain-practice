// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.

const hre = require("hardhat");

async function main() {
  // const [owner] = await ethers.getSigners();
  //  console.log(owner);

  const MyToken = await hre.ethers.getContractFactory("MyToken");
  const USDT = await hre.ethers.getContractFactory("USDT");
  const Staking = await hre.ethers.getContractFactory("Staking");

  const usdt = await USDT.deploy();
  await usdt.deployed();
  console.log(`USDT contract deployed to ${usdt.address}`);

  const myToken = await MyToken.deploy();
  await myToken.deployed();
  console.log(`MyToken contract deployed to ${myToken.address}`);

  const staking = await Staking.deploy(myToken.address, usdt.address);
  await staking.deployed();
  console.log(`Staking contract deployed to ${staking.address}`);

}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});