// ДЗ#
// Написать контракт Стейкинга 

// - Написать контракт Стейкинга
// - Написать контракт USDT-ERC20
// - Написать контракт СВОЙ ТОКЕН-ERC20
// -Использовать хардхэд
// - Задеплоить в тестовую сеть
// - Верифицировать контракты

// Требования
// - Функция BuyToken() - Покупка токена  с помощью usdt и вызывает функцию stake()
// - Функция stake()- для стейка где делается вся логика
// - Функция сlaim() - чтобы забрать %
// - Функция withdraw() - чтобы забрать СВОЙ ТОКЕН-ERC20

// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./MyToken.sol";
import "./USDT.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";

contract Staking is Ownable, Pausable {
    MyToken private myToken;
    USDT private usdt;

    struct Stake {
        uint stakedBalance;
        uint startTime;
        bool claimedReward;
    }

    mapping(address => Stake) public stakes;
    uint public percentRate = 5;

    event BuyToken(address indexed buyer, uint amount);
    event Claim(address indexed claimer, uint daysStaked, uint rewardAmount);
    event Withdraw(address indexed withdrawer, uint amount);

    constructor(address _myToken, address _usdt) {
        myToken = MyToken(_myToken);
        usdt = USDT(_usdt);
    }

    // Setter for percentRate
    function setPercentRate(uint _newPercentRate) external onlyOwner {
        percentRate = _newPercentRate;
    }

    function buyToken(uint amount) public whenNotPaused {
        require(usdt.balanceOf(msg.sender) >= amount, "Not enough tokens");
        usdt.transferFrom(msg.sender, address(this), amount);
        stake(amount);

        emit BuyToken(msg.sender, amount);
    }

    function stake(uint amount) private {
        require(stakes[msg.sender].stakedBalance == 0, "You already staked");

        stakes[msg.sender] = Stake({
            stakedBalance: amount,
            startTime: block.timestamp,
            claimedReward: false
        });
        myToken.mint(msg.sender, amount);
    }

    function claim() public whenNotPaused {
        require(!stakes[msg.sender].claimedReward, "You have already claimed reward");
        uint daysStaked = (block.timestamp - stakes[msg.sender].startTime) / 86400;
        require(daysStaked > 0, "You have not staked yet");
        uint rewardAmount = calculateReward(daysStaked, stakes[msg.sender].stakedBalance, percentRate);
        stakes[msg.sender].claimedReward = true;

        myToken.transfer(msg.sender, rewardAmount);
        emit Claim(msg.sender, daysStaked, rewardAmount);
    }

    function withdraw() public whenNotPaused {
        require(stakes[msg.sender].stakedBalance > 0, "You don't have any stakes");

        uint withdrawAmount = stakes[msg.sender].stakedBalance;
        stakes[msg.sender].stakedBalance = 0;
        usdt.transfer(msg.sender, withdrawAmount);
        myToken.transferFrom(msg.sender, address(this), withdrawAmount);

        emit Withdraw(msg.sender, withdrawAmount);
    }

    function calculateReward(uint _daysStaked, uint _stakedBalance, uint _percentRate) private pure returns (uint) {
        uint reward = _daysStaked * _stakedBalance * _percentRate / 100;
        return reward;
    }
}


