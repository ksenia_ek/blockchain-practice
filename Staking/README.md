# Staking Hardhat Project

This project demonstrates a basic Hardhat use case. It comes with a sample contract, a test for that contract, and a script that deploys that contract.

Try running some of the following tasks:

```shell
npx hardhat help
npx hardhat test
REPORT_GAS=true npx hardhat test
npx hardhat node
npx hardhat run scripts/deploy.js
```

## Верификация кода контракта на BscScan Testnet

Для использования необходимо указать `BSCSCAN_API_KEY` в файле `.env`

```console
npx hardhat verify --network bnbtestnet {contractAddress}

The contract 0xa68138aB14ad87DDee853244726E50dB6f81eAEE has already been verified
  npx hardhat verify --network bnbtestnet 0xa68138aB14ad87DDee853244726E50dB6f81eAEE "0x167D2658e4d31026C9583b0EB20119E4c64e3c54" "0x5Ed5eD76e60e50Dde292f541FE0c7d906c422D1a"
```
## Деплой
```console
npx hardhat run scripts/deploy.js --network bnbtestnet
```

