# Sample Hardhat Project

This project demonstrates a basic Hardhat use case. It comes with a sample contract, a test for that contract, and a script that deploys that contract.

Try running some of the following tasks:

```shell
npx hardhat help
npx hardhat test
REPORT_GAS=true npx hardhat test
npx hardhat node
npx hardhat run scripts/deploy.js
```

ДЗ#
Написать контракт NFT стандартов ERC-1155 совместимые с opensea. Можно наследовать паттерн у openzeppelin. Написать контракт , который должен включать в себя функции создания и продажи NFT

- Написать контракт NFT
- Загрузить какой либо файл на ipfs
- Вставить в контракт NFT ссылку на ipfs
- Написать контракт добавить функции продажи NFT за эфир сумма за один нфт 0,1. Покупать могут только кошельки из whitelist .
- Написать функцию минта для owner 
- Задеплоить в тестовую сеть
- Верифицировать контракты

Требования
- Все предусмотренные стандартами ERC-1155 функции
- Все данные NFT должны отображаться на opensea
- Функция buyNft() - Покупка нфт  нового предмета, обращается к контракту NFT и вызывает функцию mint
- Функция mint() - создание нфт овнером 
- Функция addWhitelist() - и добавляет и удаляет с вайтлиста
