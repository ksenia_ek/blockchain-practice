// Написать контракт NFT стандартов ERC-1155 совместимые с opensea. Можно наследовать паттерн у openzeppelin. Написать контракт , который должен включать в себя функции создания и продажи NFT

// - Написать контракт NFT
// - Загрузить какой либо файл на ipfs
// - Вставить в контракт NFT ссылку на ipfs
// - Написать контракт добавить функции продажи NFT за эфир сумма за один нфт 0,1. Покупать могут только кошельки из whitelist .
// - Написать функцию минта для owner 
// - Задеплоить в тестовую сеть
// - Верифицировать контракты

// Требования
// - Все предусмотренные стандартами ERC-1155 функции
// - Все данные NFT должны отображаться на opensea
// - Функция buyNft() - Покупка нфт  нового предмета, обращается к контракту NFT и вызывает функцию mint
// - Функция mint() - создание нфт овнером 
// - Функция addWhitelist() - и добавляет и удаляет с вайтлиста

// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/Ownable.sol";


contract MyERC1155 is ERC1155, Ownable {
    uint256 public constant tokenId = 0;
    uint256 private payment = 0.1 ether;
    //100000000000000000 wei
    //100000000 gwei
    //0.1 ether;
    string public constant name = "Pixel Cat and Sunset";

    mapping(address => bool) public isWhitelisted;

     constructor() ERC1155("ipfs://QmTRN6siT5b4X3fvGMfNWJ4NeT55w7Y1nuCj5RqagWFnB5") {
     }

    function mint(address _to, uint256 _amount) public onlyOwner {
        require(_to != address(0), "Cannot mint to zero address");
        bytes memory data;
        _mint(_to, tokenId, _amount, data);
    }

    function addWhitelist(address account) public onlyOwner {
        require(!isWhitelisted[account], "Account already whitelisted");
        isWhitelisted[account] = true;
    }

    function removeWhitelist(address account) public onlyOwner {
        require(isWhitelisted[account], "Account not whitelisted");
        isWhitelisted[account] = false;
    }

    function buyNft() external payable {
        require(isWhitelisted[msg.sender], "Caller is not in whitelist");
        require(msg.value >= payment, "Not enough ether to purchase NFT");
        uint256 refund = msg.value - payment;
        if (refund > 0) {
            payable(msg.sender).transfer(refund);
        }
        _mint(msg.sender, tokenId, 1, "");
    }

    //set payment
    function setPayment(uint256 _payment) public onlyOwner {
        payment = _payment;
    }

    //set new URI
     function setURI(string memory newuri) public onlyOwner {
        _setURI(newuri);
    }

}
